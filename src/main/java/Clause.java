/**
* Created by raze on 17.08.14.
*/
public class Clause {
    final int var1, var2;

    public Clause(int var1, int var2) {
        this.var1 = var1;
        this.var2 = var2;
    }
}
