import com.google.common.collect.ImmutableSet;

/**
 * Created by raze on 17.08.14.
 */
public class TwoSat {

    private final TwoSatGraph graph;
    private final int varCount;
    private final KosarajuSharirSCC scc;
    private final Digraph sccGraph;

    public TwoSat(int varCount, Iterable<Clause> clauses) {
        this.varCount = varCount;
        int nodesCount = varCount * 2;

        graph = new TwoSatGraph(nodesCount);
        for (Clause clause : clauses) {
            graph.addClause(clause);
        }
        scc = new KosarajuSharirSCC(graph);
        sccGraph = new Digraph(scc.count());

        for (int nodeFrom = 0; nodeFrom < nodesCount; nodeFrom++) {
            for (int nodeTo : graph.adj(nodeFrom)) {
                int cc1 = scc.id(nodeFrom);
                int cc2 = scc.id(nodeTo);
                if (cc1 != cc2 && !ImmutableSet.copyOf(sccGraph.adj(cc1)).contains(cc2)) {
                    sccGraph.addEdge(cc1, cc2);
                }
            }
        }
    }

    public boolean isSatisfiable() {
        for (int var = 0; var < varCount; var++) {
            int oppositeVar = graph.getOppositeVar(var);
            int oppositeVarScc = scc.id(oppositeVar);
            int varScc = scc.id(var);

            if (isOppositeVarReachable(varScc, oppositeVarScc) &&
                    isOppositeVarReachable(oppositeVarScc, varScc)) {
                return false;
            }
        }
        return true;
    }

    private boolean isOppositeVarReachable(int nodeScc, int oppositeVarScc) {
        if (nodeScc == oppositeVarScc)
            return true;

        for (int scc : sccGraph.adj(nodeScc)) {
            if (isOppositeVarReachable(scc, oppositeVarScc))
                return true;
        }

        return false;
    }

}
