/**
 * Created by raze on 17.08.14.
 */
public class TwoSatGraph extends Digraph {

    public TwoSatGraph(int V) {
        super(V);
    }

    public void addClause(Clause clause) {
        addEdge(getEffectiveIndex(-clause.var1), getEffectiveIndex(clause.var2));
        addEdge(getEffectiveIndex(-clause.var2), getEffectiveIndex(clause.var1));
    }

    public int getOppositeVar(int var) { // TODO poor semantics and implementation (2 govnocoders:))
        return var + V() / 2;
    }

    private int getEffectiveIndex(int idx) {
        return (idx > 0 ? idx : -idx + V() / 2) - 1;
    }

}
