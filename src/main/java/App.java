import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by raze on 17.08.14.
 */
public class App {
    public static void main(String[] args) throws URISyntaxException, IOException {
        String input = "test3";
        for (int i = 1; i <= 6; i++) {
            input = "2sat" + i + ".txt";

            List<String> lines = Files.readAllLines(Paths.get(App.class.getResource(input).toURI()));
            int count = Integer.valueOf(lines.get(0));

            List<Clause> clauses = lines.stream().skip(1).map((line) -> {
                int[] tuple = Arrays.asList(line.split(" ")).stream().mapToInt(Integer::valueOf).toArray();
                return new Clause(tuple[0], tuple[1]);
            }).collect(Collectors.toList());

            boolean isSatisfiable = new TwoSat(count, clauses).isSatisfiable();



            System.out.println(i + ": " + "isSatisfiable = " + isSatisfiable);
        }
    }

}
